//
//  ViewController.swift
//  Calculator
//
//  Created by Eldar on 27.03.2021.
//

import UIKit
import SnapKit

class ViewController: UIViewController {

    var fieldOne = UITextField()
    var fieldTwo = UITextField()

    var resultLabel = UILabel()
    
    var plus = UIButton()
    var minus = UIButton()
    var del = UIButton()
    var umn = UIButton()
    var pro = UIButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        resultLabel.text = "0"
        resultLabel.textColor = UIColor.black
        
        view.addSubview(resultLabel)
        resultLabel.snp.makeConstraints { (make) in
            make.center.equalTo(view.snp.center)
        }
        
        plus.backgroundColor = UIColor.black
        plus.setTitle("+", for: .normal)
        plus.addTarget(self, action: #selector(clockPlus), for: UIControl.Event.touchUpInside)
        
        view.backgroundColor = UIColor.orange
        
        view.addSubview(plus)
        plus.snp.makeConstraints { (make) in
            make.height.equalTo(60)
            make.width.equalTo(60)
            make.centerX.equalTo(view.snp.centerX)
            make.top.equalTo(resultLabel.snp.bottom).offset(20)
        }
        
        minus.backgroundColor = UIColor.black
        minus.setTitle("-", for: .normal)
        minus.addTarget(self, action: #selector(clockMinus), for: UIControl.Event.touchUpInside)
        
        view.addSubview(minus)
        minus.snp.makeConstraints { (make) in
            make.height.equalTo(60)
            make.width.equalTo(60)
            make.left.equalTo(plus.snp.right).offset(20)
            make.top.equalTo(resultLabel.snp.bottom).offset(20)
        }
        
        del.backgroundColor = UIColor.black
        del.setTitle("/", for: .normal)
        del.addTarget(self, action: #selector(clockDel), for: UIControl.Event.touchUpInside)
        
        view.addSubview(del)
        del.snp.makeConstraints { (make) in
            make.height.equalTo(60)
            make.width.equalTo(60)
            make.right.equalTo(plus.snp.left).offset(-20)
            make.top.equalTo(resultLabel.snp.bottom).offset(20)
        }
        
        umn.backgroundColor = UIColor.black
        umn.setTitle("*", for: .normal)
        umn.addTarget(self, action: #selector(clockUmn), for: UIControl.Event.touchUpInside)
        
        view.addSubview(umn)
        umn.snp.makeConstraints { (make) in
            make.height.equalTo(60)
            make.width.equalTo(60)
            make.centerX.equalTo(view.snp.centerX)
            make.top.equalTo(plus.snp.bottom).offset(20)
        }
       
        pro.backgroundColor = UIColor.black
        pro.setTitle("%", for: .normal)
        pro.addTarget(self, action: #selector(clockPro), for: UIControl.Event.touchUpInside)
        
        view.addSubview(pro)
        pro.snp.makeConstraints { (make) in
            make.height.equalTo(60)
            make.width.equalTo(60)
            make.centerX.equalTo(view.snp.centerX)
            make.top.equalTo(umn.snp.bottom).offset(20)
        }
        
        
        view.addSubview(fieldOne)
        view.addSubview(fieldTwo)
        
        fieldOne.snp.makeConstraints { (make) in
            make.left.equalTo(view.snp.left)
            make.width.equalTo(view.frame.width / 2.0)
            make.bottom.equalTo(resultLabel.snp.top).offset(-20)
        }
        
        fieldTwo.snp.makeConstraints { (make) in
            make.right.equalTo(view.snp.right)
            make.width.equalTo(view.frame.width / 2.0)
            make.bottom.equalTo(resultLabel.snp.top).offset(-20)
        }
    }
    
    @objc func clockPlus(view: UIButton) {
        let numberOne: Double = Double(fieldOne.text ?? "0") ?? 0
        let numberTwo: Double = Double(fieldTwo.text ?? "0") ?? 0
        
        resultLabel.text = String(numberOne + numberTwo)
    }
    
    @objc func clockMinus(view: UIButton) {
        let numberOne: Double = Double(fieldOne.text ?? "0") ?? 0
        let numberTwo: Double = Double(fieldTwo.text ?? "0") ?? 0
        
        resultLabel.text = String(numberOne - numberTwo)
    }
    
    @objc func clockDel(view: UIButton) {
        let numberOne: Double = Double(fieldOne.text ?? "0") ?? 0
        let numberTwo: Double = Double(fieldTwo.text ?? "0") ?? 0
        
        resultLabel.text = String(numberOne / numberTwo)
    }
    
    @objc func clockUmn(view: UIButton) {
        let numberOne: Double = Double(fieldOne.text ?? "0") ?? 0
        let numberTwo: Double = Double(fieldTwo.text ?? "0") ?? 0
        
        resultLabel.text = String(numberOne * numberTwo)
    }
    
    @objc func clockPro(view: UIButton) {
        let numberOne: Double = Double(fieldOne.text ?? "0") ?? 0
        let numberTwo: Double = Double(fieldTwo.text ?? "0") ?? 0
        
        resultLabel.text = String((numberOne / 100) /  numberTwo)
    }
}

